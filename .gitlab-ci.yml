include: VersionInfo.yml

stages:
  - build
  - pack
  - deploy

variables:
  REVISION_NUMBER: ${RELEASE_NUMBER}_${CI_COMMIT_SHORT_SHA}
  EXPORT_NAME_RELEASE: ${GAME_NAME}_${RELEASE_NUMBER}
  EXPORT_NAME_INTERNAL: ${GAME_NAME}_${RELEASE_NUMBER}_${CI_COMMIT_SHORT_SHA}
  GIT_SUBMODULE_STRATEGY: recursive


# =+=+=+=+=+=+=+= BRIDGE JOBS =+=+=+=+=+=+=+=
.bridge: &bridge
  stage: build
  inherit:
    variables: false
  trigger:
    project: $PROJECT_NAME
    branch: main
    strategy: depend

.bridge-repack: &bridge-repack
  image: alpine/curl:latest
  retry: 2
  artifacts:
    paths:
      - Windows
      - Mac
      - Linux
  script:
  - mkdir -v -p Windows
  - mkdir -v -p Mac
  - mkdir -v -p Linux
  - curl --location --output ./$SUBGAME_NAME.zip "https://gitlab.com/20-games-in-30-days/$SUBGAME_NAME/-/jobs/artifacts/main/download?job=consolidateArtifacts&job_token=$CI_JOB_TOKEN"
  - unzip ./$SUBGAME_NAME.zip -d $SUBGAME_NAME
  - mv $SUBGAME_NAME/build/windows-release/* Windows/
  - '[ "$(ls -A Windows)" ] || (echo "Error: Windows is empty!" && exit 1)'
  - mv $SUBGAME_NAME/build/linux-release/* Linux/
  - '[ "$(ls -A Linux)" ] || (echo "Error: Linux is empty!" && exit 1)'
  - mv $SUBGAME_NAME/build/mac-release/* Mac/
  - '[ "$(ls -A Mac)" ] || (echo "Error: Mac is empty!" && exit 1)'


# ================================================================================================================
#                                                   Job Dependencies
# ================================================================================================================

# =+=+=+=+=+=+=+= CHECK FOR MISSING ARTIFACTS =+=+=+=+=+=+=+=
# https://gitlab.com/gitlab-org/gitlab/-/issues/22711
# This might be a configuration issue with my runner, but files are
# missing even though a job is marked as complete. We need to manually
# inspect the files from every single job. It sucks, but the GitLab
# team assures us that this is a feature, not a bug :(
#
# File count is based on the current number of jobs: 14 games = 14 for Mac, 28 for Windows and Linux.
.check_lost_files: &check_lost_files
  script:
    - ls ./Windows
    - '[ $(find ./Windows -maxdepth 1 -type f | wc -l) -eq 28 ] || exit 1'
    - ls ./Linux
    - '[ $(find ./Linux -maxdepth 1 -type f | wc -l) -eq 28 ] || exit 1'
    - ls ./Mac
    - '[ $(find ./Mac -maxdepth 1 -type f | wc -l) -eq 14 ] || exit 1'


# =+=+=+=+=+=+=+= CONSOLIDATE COMMON =+=+=+=+=+=+=+=
.consolidate_artifacts: &consolidate_artifacts
  image: alpine:latest
  stage: deploy
  script: 
    - "ls ./$PLATFORM"
  artifacts:
    name: $GAME_NAME_$REVISION_NUMBER
    expose_as: "Build Output"
    paths:
      - ./$PLATFORM/
    expire_in: 1 day # This is huge, we probably want to save it to a repo somewhere so we don't run out of space.
  except: 
    - /^release.*/


# =+=+=+=+=+=+=+= DEPLOY COMMON =+=+=+=+=+=+=+=
.itch_deploy: &itch_deploy
  image: barichello/godot-ci:$GODOT_VERSION
  stage: deploy
  script:
    - butler push ./Windows $GROUP_NAME/$GAME_NAME:Windows
    - butler push ./Linux $GROUP_NAME/$GAME_NAME:Linux
    - butler push ./Mac $GROUP_NAME/$GAME_NAME:Mac

# =+=+=+=+=+=+=+= DEPLOY SETTINGS =+=+=+=+=+=+=+=
.needs_release: &needs_release
  needs:
    - repack:template
    - repack:pong
    - repack:breakout
    - repack:space-invaders
    - repack:asteroids
    - repack:frogger
    - repack:pac-man
    - repack:social-clicker
    - repack:jump-man
    - repack:motherload
    - repack:minecraft
    - repack:mario-kart
    - repack:hydro-thunder
    - repack:pinball
    - repack:survivors
    - repack:doom

# ================================================================================================================
#                                                           Jobs
# ================================================================================================================
# =+=+=+=+=+=+=+= BRIDGE JOBS =+=+=+=+=+=+=+=

# Template
bridge:template:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/godot-4-game-template

repack:template:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:template"]
  variables:
    SUBGAME_NAME: godot-4-game-template

# Pong
bridge:pong:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/pong

repack:pong:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:pong"]
  variables:
    SUBGAME_NAME: pong

# Breakout
bridge:breakout:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/breakout

repack:breakout:
  stage: pack
  needs: ["bridge:breakout"]
  <<: *bridge-repack
  variables:
    SUBGAME_NAME: breakout

# Space Invaders
bridge:space-invaders:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/space-invaders

repack:space-invaders:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:space-invaders"]
  variables:
    SUBGAME_NAME: space-invaders

# Asteroids
bridge:asteroids:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/asteroids

repack:asteroids:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:asteroids"]
  variables:
    SUBGAME_NAME: asteroids

# Frogger
bridge:frogger:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/frogger

repack:frogger:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:frogger"]
  variables:
    SUBGAME_NAME: frogger

# Pac Man
bridge:pac-man:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/pac-man

repack:pac-man:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:pac-man"]
  variables:
    SUBGAME_NAME: pac-man

# Cookie Clicker
bridge:social-clicker:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/social-clicker

repack:social-clicker:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:social-clicker"]
  variables:
    SUBGAME_NAME: social-clicker

# Mario
bridge:jump-man:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/jump-man

repack:jump-man:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:jump-man"]
  variables:
    SUBGAME_NAME: jump-man

# Motherload
bridge:motherload:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/motherload

repack:motherload:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:motherload"]
  variables:
    SUBGAME_NAME: motherload

# Minecraft
bridge:minecraft:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/minecraft

repack:minecraft:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:minecraft"]
  variables:
    SUBGAME_NAME: minecraft

# Mario Kart
bridge:mario-kart:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/mario-kart

repack:mario-kart:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:mario-kart"]
  variables:
    SUBGAME_NAME: mario-kart

# Hydro Thunder
bridge:hydro-thunder:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/hydro-thunder

repack:hydro-thunder:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:hydro-thunder"]
  variables:
    SUBGAME_NAME: hydro-thunder

# Pinball
bridge:pinball:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/pinball

repack:pinball:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:pinball"]
  variables:
    SUBGAME_NAME: pinball

# Survivors
bridge:survivors:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/survivors

repack:survivors:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:survivors"]
  variables:
    SUBGAME_NAME: survivors

# Doom
bridge:doom:
  <<: *bridge
  variables:
    PROJECT_NAME: 20-games-in-30-days/doom

repack:doom:
  <<: *bridge-repack
  stage: pack
  needs: ["bridge:doom"]
  variables:
    SUBGAME_NAME: doom

# =+=+=+=+=+=+=+= PACK JOBS =+=+=+=+=+=+=+=
# Contain all of the builds in a single artifact so we don't have to download all the things!
consolidateArtifacts:Windows:
  <<: *check_lost_files
  <<: *needs_release
  <<: *consolidate_artifacts
  variables:
    PLATFORM: Windows

consolidateArtifacts:Linux:
  <<: *check_lost_files
  <<: *needs_release
  <<: *consolidate_artifacts
  variables:
    PLATFORM: Linux

consolidateArtifacts:Mac:
  <<: *check_lost_files
  <<: *needs_release
  <<: *consolidate_artifacts
  variables:
    PLATFORM: Mac


# =+=+=+=+=+=+=+= DEPLOY JOBS =+=+=+=+=+=+=+=
itchio-release:
  <<: *check_lost_files
  <<: *needs_release
  <<: *itch_deploy
  variables:
    STREAM_SUFFIX: ${RELEASE_STREAM_SUFFIX}
  only:
    - master
  when: manual